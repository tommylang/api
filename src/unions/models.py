from django.db import models

from unions.validators import validate_document_number
from address.models import AddressField


class AbstractBaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        abstract = True
        default_permissions = ()


class Activity(AbstractBaseModel):
    """ Business activity model

    Attributes:
        name (CharField): name of the activity

    """
    code = models.CharField(max_length=20)
    name = models.CharField(max_length=200)


class Enterprise(AbstractBaseModel):
    TYPE_UNDEFINED = 'undefined'
    TYPES = (
        (TYPE_UNDEFINED, 'Sin Definir'),
        ('eirl', 'EIRL'),
        ('ltda', 'LTDA'),
        ('sa', 'SA'),
        ('spa', 'SPA')
    )

    document_number = models.CharField(max_length=20, unique=True, blank=False, null=False,
                                       validators=[validate_document_number])
    type = models.CharField(max_length=10, choices=TYPES, default=TYPE_UNDEFINED)
    business_name = models.CharField(max_length=200)
    fantasy_name = models.CharField(max_length=200, blank=True)
    activities = models.ManyToManyField(Activity, blank=True)


class Union(AbstractBaseModel):
    TYPE_UNDEFINED = 'undefined'
    TYPES = (
        (TYPE_UNDEFINED, 'Sin Definir'),
        ('sindicato', 'Sindicato'),
        ('federacion', 'Federación de Sindicatos'),
        ('confederacion', 'Confederación de Sindicatos'),
    )

    name = models.CharField(max_length=60)
    enterprise = models.ForeignKey(Enterprise, on_delete=models.CASCADE)
    type = models.CharField(max_length=10, choices=TYPES, default=TYPE_UNDEFINED)
    constitution_date = models.DateField(blank=True, null=True)
    number = models.IntegerField(default=True)
    email = models.CharField(max_length=100, blank=False, null=True)
    address = AddressField(on_delete=models.CASCADE)
