import re
from itertools import cycle

from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


country_regex = {
    'MX': r'^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3}$)',
    'CL': r'^\d{1,3}(?:\.?\d{3}){2}-?[\dkK]$'
}


def validate_document_number(document_number):
    """
    Validation for the document number

    :param document_number
    :raise ValidationError

    """
    country = settings.COUNTRY_PREFIX
    if country not in country_regex:
        raise ValidationError(_('Unknown country'), code='unknown')

    document_number_regex = re.compile(country_regex[country])
    if document_number_regex.match(document_number):
        if country == 'CL':
            _validate_check_digit(document_number)

    else:
        raise ValidationError(_('Invalid document number'), code='invalid')


def _validate_check_digit(document_number):
    """
    Validation for the document number check digit(Chilean case)

    :param document_number
    :return True/False

     """

    clean_document_number = document_number.replace(".", "").replace("-", "").upper()

    original_dv = clean_document_number[-1:]
    digits = clean_document_number[:-1]

    reversed_digits = map(int, reversed(str(digits)))
    factors = cycle(range(2, 8))
    calculated_dv = sum(d * f for d, f in zip(reversed_digits, factors))
    calculated_dv = (-calculated_dv) % 11
    calculated_dv = 'K' if calculated_dv == 10 else str(calculated_dv)

    if original_dv != calculated_dv:
        raise ValidationError(_('The check digit isn\'t a valid value'), code='invalid')
