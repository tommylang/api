from django.db import models
from django.contrib.auth.models import User
from address.models import AddressField

from unions.models import Union


class AbstractBaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        abstract = True
        default_permissions = ()


class UserDetail(AbstractBaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    union = models.ForeignKey(Union, on_delete=models.CASCADE)
    email = models.EmailField(unique=True, blank=False)
    address = AddressField()
