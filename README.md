# Levantar API localmente
### Crear docker-compose.yml

Se debe hacer solo si no lo han creado o lo quieran reemplazar
```
cd scripts
sh create_docker_compose.sh
cd ..
```

### Descargar dumps reciente

Editar archivo docker-compose.yml y setear los valores de las llaves de AWS

```
docker-compose up aws
```

### Levantar DB local

```
docker-compose up -d db
```

Se creará una carpeta ./volumes/mysql, esta carpeta sirve para tener la data persistente, si se quiere cargar el dumps nuevamente, solo se debe eliminar.

### Levantar API local

```
docker-compose up -d api
```

# Dudas?

Solo Pregunta!