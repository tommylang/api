echo 'version: "2.4"
services:
  api:
    extends:
      file: docker-compose.base.yml
      service: api
    environment:
      - AWS_ACCESS_KEY_ID=anotherkey
      - AWS_REGION_NAME=anotherkey
      - AWS_SECRET_ACCESS_KEY=anotherkey
    depends_on:
      - db
  db:
    extends:
      file: docker-compose.base.yml
      service: db
  aws:
    extends:
      file: docker-compose.base.yml
      service: aws
    environment:
      - AWS_ACCESS_KEY_ID=anotherkey
      - AWS_REGION_NAME=anotherkey
      - AWS_SECRET_ACCESS_KEY=anotherkey' > ../docker-compose.yml
